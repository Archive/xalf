/*
 * xalf_launch - overloaded Xlib function XMapWindow and XMapRaised
 * 
 * To be used with xalf. 
 *
 * Peter �strand <astrand@lysator.liu.se> 2001. GPLV2. 
 *
 * Based on scwm_set_pid_property.c (C) 1999 Toby Sargeant and Greg J. Badros
 * 
 * This library is hopefully threadsafe. Please report bugs. 
 * */
 
/* Uncomment below for debugging */
/* #define DEBUG */

#ifdef DEBUG
#   define DPRINTF(args) fprintf args
#else
#   define DPRINTF(args) 
#endif

#define _GNU_SOURCE

#include "config.h"
#include <dlfcn.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>

#define PID_ENV_NAME "XALF_LAUNCH_PID"
#ifdef __sgi
    #define NUM_ABI 3
    static char *saved_preload_name[NUM_ABI] = { "XALF_SAVED_PRELOAD",
    						 "XALF_SAVED_PRELOADN32",
    						 "XALF_SAVED_PRELOAD64" }; 
    static char *saved_preload_set[NUM_ABI] = { "XALF_SAVED_PRELOAD=",
    						"XALF_SAVED_PRELOADN32=",
    						"XALF_SAVED_PRELOAD64=" }; 
    static char *ld_preload[NUM_ABI] = { "_RLD_LIST", "_RLDN32_LIST", "_RLD64_LIST" };
    static char *ld_preload_set[NUM_ABI] = { "_RLD_LIST=", "_RLDN32_LIST=", "_RLD64_LIST=" };
#else
    #define NUM_ABI 1
    static char *saved_preload_name[NUM_ABI] = { "XALF_SAVED_PRELOAD" };
    static char *saved_preload_set[NUM_ABI] = { "XALF_SAVED_PRELOAD=" };
    static char *ld_preload[NUM_ABI] = { "LD_PRELOAD" };
    static char *ld_preload_set[NUM_ABI] = { "LD_PRELOAD=" };
#endif


/* Prototypes */
static void restore_env();
static pid_t launch_pid = 0;


/* Init function. I wish there were some better way of doing this... */
#ifdef __FreeBSD__
    static void _init(void) __attribute__ ((section (".init")));
#endif /* __FreeBSD__ */

#if defined __GNUC__ && ( ( __GNUC__ == 2 ) && ( __GNUC_MINOR__ >= 96) || ( __GNUC__ >= 3) )
    void initialize (void) __attribute__ ((constructor));
    void initialize (void)
#else
    void
    _init ()
#endif
{
    char *pid_string = NULL;
    void *dlh = NULL;

    DPRINTF ((stderr, "libxalflaunch: _init\n"));

    pid_string = getenv (PID_ENV_NAME);
    if (pid_string) 
	launch_pid = atol (pid_string);
    DPRINTF ((stderr, "libxalflaunch: launch_pid is %ld\n", (long)launch_pid));

    if (launch_pid == (pid_t)-1) 
	{
	    /* The xalf wrapper was only testing for our existence. */
	    DPRINTF ((stderr, "libxalflaunch: Exiting immediately from _init()\n"));
	    return;
	}
    else if (launch_pid == 0) 
	{
	    /* PID_PROPERTY_NAME was not defined. 
	       Restore env, so that we don't get loaded again. */
 	    restore_env ();
	    return;
	}

    dlh = dlopen (NULL, RTLD_GLOBAL | RTLD_NOW);
    if (dlsym (dlh, "XSync") == NULL) 
	{
	    /* This is not an X11 app. Maybe our app is started via a shellscript. 
	       Hold on... */
	    DPRINTF ((stderr, "libxalflaunch: No XSync\n"));
	    return;
	}
    
    restore_env ();
}


extern int 
XMapWindow (Display* display, Window w)                               
{                                                                 
    /* declare fptr as static pointer to function returning int */              
    static int (*fptr)() = 0;

    DPRINTF ((stderr, "libxalflaunch: XMapWindow\n"));

    /* we don't want to kill neither 0 nor init */
    if (launch_pid > 1) 
	{
	    DPRINTF ((stderr, "libxalflaunch: Sending signal to process %d\n", (long)launch_pid));
	    kill (launch_pid, SIGUSR1);
	    launch_pid = 0;
	}
    
    if (fptr == 0) 
	/* This is the first call to XMapWindow. Fetch function pointer. */
	{
#ifdef RTLD_NEXT
	    fptr = (int (*)())dlsym (RTLD_NEXT, "XMapWindow");
#else
	    /* This platform does not support RTLD_NEXT (Irix, for example). 
	       Use dlopen() instead. */
	    void *dlh = NULL;

	    dlh = dlopen ("libX11.so", RTLD_GLOBAL | RTLD_NOW);
	    if (dlh == NULL) 
		dlh = dlopen ("libX11.so.6", RTLD_GLOBAL | RTLD_NOW); 
	    if (dlh == NULL)
		fprintf (stderr, "libxalflaunch: %s\n", dlerror ());
	    if (dlh != NULL) {
		fptr = (int (*)())dlsym (dlh, "XMapWindow");
	    }
	    
	    DPRINTF ((stderr, "libxalflaunch: XMapWindow is at %p\n", fptr));
#endif
	    if (fptr == NULL) 
		{
		    fprintf (stderr, "libxalflaunch: dlsym: %s\n", dlerror());
		    /* Zero means an error in Xlib */
		    return 0;
		}
	}

    /* Call XMapWindow and return result. */
    return (*fptr)(display, w);
}


extern int 
XMapRaised (Display* display, Window w)                               
{                            
    /* declare fptr as static pointer to function returning int */
    static int (*fptr)() = 0;

    DPRINTF ((stderr, "libxalflaunch: XMapRaised\n"));

    /* we don't want to kill neither 0 nor init */
    if (launch_pid > 1) 
	{
	    DPRINTF ((stderr, "libxalflaunch: Sending signal to process %d\n", (long)launch_pid));
	    kill (launch_pid, SIGUSR1);
	    launch_pid = 0;
	}
    
    if (fptr == 0) 
	/* This is the first call to XMapRaised. Fetch function pointer. */
	{
#ifdef RTLD_NEXT
	    fptr = (int (*)())dlsym (RTLD_NEXT, "XMapRaised");
#else
	    /* This platform does not support RTLD_NEXT (Irix, for example). 
	       Use dlopen() instead. */
	    void *dlh = NULL;

	    dlh = dlopen ("libX11.so", RTLD_GLOBAL | RTLD_NOW);
	    if (dlh == NULL) 
		dlh = dlopen ("libX11.so.6", RTLD_GLOBAL | RTLD_NOW); 
	    if (dlh == NULL)
		fprintf (stderr, "libxalflaunch: %s\n", dlerror ());
	    if (dlh != NULL) {
		fptr = (int (*)())dlsym (dlh, "XMapRaised");
	    }
	    
	    DPRINTF ((stderr, "libxalflaunch: XMapRaised is at %p\n", fptr));
#endif
	    if (fptr == NULL) 
		{
		    fprintf (stderr, "libxalflaunch: dlsym: %s\n", dlerror());
		    /* Zero means an error in Xlib */
		    return 0;
		}
	}

    /* Call XMapRaised and return result. */
    return (*fptr)(display, w);
}


#ifdef HAVE_UNSETENV
    #define PRELOAD_UNSETTER unsetenv (ld_preload[i]);
    #define SAVED_PRELOAD_UNSETTER unsetenv (saved_preload_name[i]);
    #define PID_UNSETTER unsetenv (PID_ENV_NAME);
#else
    #define PRELOAD_UNSETTER \
        if (putenv (ld_preload_set[i])) \
	    fprintf (stderr, "libxalflaunch: unsetting %s failed\n", ld_preload[i]);
    #define SAVED_PRELOAD_UNSETTER \
        if (putenv (saved_preload_set[i])) \
            fprintf (stderr, "libxalflaunch: unsetting %s failed\n", saved_preload_name[i]);
    #define PID_UNSETTER \
    	if (putenv (PID_ENV_NAME"=")) \
    	    fprintf (stderr, "libxalflaunch: unsetting "PID_ENV_NAME" failed\n");
#endif /* HAVE_UNSETENV */


/* Unset or restore LD_PRELOAD
   and unset PID_ENV_NAME */
static void restore_env()
{
    char *saved_preload = NULL;
    char *new_preload = NULL;
    int i;

    for (i = 0; i < NUM_ABI; i++) {
	saved_preload = getenv (saved_preload_name[i]);
	
	if (saved_preload)
	    /* LD_PRELOAD was set before Xalf was called. Restore it. */
	    {
		DPRINTF ((stderr, "libxalflaunch: Restoring %s%s\n", 
			  ld_preload_set[i], saved_preload));
		new_preload = malloc (strlen (saved_preload) + \
				      strlen(ld_preload_set[i]) + 1);
		if (new_preload == NULL)
		    /* Malloc failed. */
		    {
			fprintf (stderr, "libxalflaunch: malloc failed\n");
		    }
		else 
		    {
			strcpy (new_preload, ld_preload_set[i]);
			strcat (new_preload, saved_preload);
			/* Note: new_preload becomes a part of the new environment, 
			   and should not be free:d. */
			if (putenv (new_preload)) 
			    fprintf (stderr, "libxalflaunch: putenv failed\n");
		    }
		
		/* Unset the saved LD_PRELOAD. */
		SAVED_PRELOAD_UNSETTER;
	    }
	else
	    {
		DPRINTF ((stderr, "libxalflaunch: Unsetting LD_PRELOAD\n"));
		PRELOAD_UNSETTER;
	    }
    }
    
    /* Unset PID_ENV_NAME */
    PID_UNSETTER;
}

